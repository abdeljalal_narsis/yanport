package helper;

import model.grid.Position;

public class PositionHelper {

    private PositionHelper(){}

    public static Position calculateNewPosition(Position position, int x, int y){
        if (position == null)
            return null;
        return new Position(position.getX() + x, position.getY() + y);
    }
}
