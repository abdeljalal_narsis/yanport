package model.grid.item;

import model.enums.Orientation;
import model.grid.AbstractItem;

public class Vacuum extends AbstractItem {

    public Vacuum(Orientation orientation)
    {
        super(orientation);
    }

}
