package model.enums;

public enum Orientation {
    N ('N'),
    S('S'),
    E('E'),
    W('W');

    private final char name;

    Orientation(char name) {
        this.name = name;
    }

    public static Orientation getOrientationForName(final char name)
    {
        for (Orientation type : Orientation.values())
            if (type.name == name)
                return type;
        return null;
    }
}
