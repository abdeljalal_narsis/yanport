package model.grid;

import helper.PositionHelper;
import model.enums.Orientation;

public abstract class AbstractItem {

    private Position position;
    private Orientation orientation;

    public AbstractItem(Orientation orientation)
    {
        this.orientation=orientation;
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation(){
        return orientation;
    }

    void setPosition(Position position) {
        this.position = position;
    }

    public void rotateLeft(){
        switch (orientation)
        {
            case N:
                orientation = Orientation.W;
                break;
            case S:
                orientation = Orientation.E;
                break;
            case E:
                orientation = Orientation.N;
                break;
            case W:
                orientation = Orientation.S;
                break;
        }
    }

    public void rotateRight(){
        switch (orientation)
        {
            case N:
                orientation = Orientation.E;
                break;
            case S:
                orientation = Orientation.W;
                break;
            case E:
                orientation = Orientation.S;
                break;
            case W:
                orientation = Orientation.N;
                break;
        }
    }

    public Position calculateMovement()
    {
        int x = 0;
        int y = 0;
        switch (orientation)
        {
            case N:
                y+=1;
                break;
            case S:
                y-=1;
                break;
            case E:
                x+=1;
                break;
            case W:
                x-=1;
                break;
        }
        return PositionHelper.calculateNewPosition(this.position, x, y);
    }
}
