package model.grid;

import model.enums.Orientation;
import model.grid.item.Vacuum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

public class GridTest {

    @Test
    void addItemToGridTest(){
        Grid grid = new Grid(10,10);
        AbstractItem item = new Vacuum(Orientation.N);
        grid.addItemToGrid(item,5,5);
        Map<Position, AbstractItem> positionsCopy = grid.getPositionsCopy();
        Assertions.assertEquals(item, positionsCopy.get(new Position(5,5)));
    }

    @Test
    void addItemToGridExceptionTest(){
        Grid grid = new Grid(10,10);
        AbstractItem item = new Vacuum(Orientation.N);
        Assertions.assertThrows(IllegalArgumentException.class, () -> grid.addItemToGrid(item,11,11));
    }

    @Test
    void moveItemTest(){
        Grid grid = new Grid(10,10);
        AbstractItem item = new Vacuum(Orientation.N);
        grid.addItemToGrid(item,5,5);
        grid.moveItem(item,"DADADADAA");
        Map<Position, AbstractItem> positionsCopy = grid.getPositionsCopy();
        Assertions.assertEquals(item, positionsCopy.get(new Position(5,6)));
        Assertions.assertEquals(item.getOrientation(), Orientation.N);
        Assertions.assertEquals(item.getPosition(), new Position(5,6));
        Assertions.assertNull(positionsCopy.get(new Position(5, 5)));
    }

    @Test
    void moveItemExceptionOutOfGridTest(){
        Grid grid = new Grid(10,10);
        AbstractItem item = new Vacuum(Orientation.N);
        grid.addItemToGrid(item,5,5);
        Assertions.assertThrows(IllegalArgumentException.class, () -> grid.moveItem(item,"AAAAAAAAAAAAA"));
    }

}
