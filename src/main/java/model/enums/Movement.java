package model.enums;

public enum Movement {
    D('D'),
    G('G'),
    A('A');

    private final char name;

    Movement(char name) {
        this.name = name;
    }

    public static Movement getMovementForName(final char name)
    {
        for (Movement type : Movement.values())
            if (type.name == name)
                return type;
        return null;
    }
}
