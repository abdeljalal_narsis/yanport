package model.grid;


import model.enums.Movement;

import java.util.HashMap;
import java.util.Map;

import static helper.ParseHelper.parseMovement;

public class Grid {

    private Map<Position, AbstractItem> positions = new HashMap<>();

    public Grid(int x, int y)
    {
        for(int i=0;i<x;i++)
            for(int j=0;j<y;j++)
                positions.put(new Position(i,j), null);
    }

    public void addItemToGrid(AbstractItem item, int x, int y) throws IllegalArgumentException
    {
        Position p = new Position(x, y);
        if(!positions.containsKey(p))
            throw new IllegalArgumentException("La grille ne contient pas la position donnée");
        item.setPosition(p);
        positions.put(p,item);
    }

    public void moveItem(AbstractItem item, String instructions) throws IllegalArgumentException
    {
        moveItem(item, instructions.toCharArray());
    }

    public void moveItem(AbstractItem item, char[] instructions) throws IllegalArgumentException
    {
        for(char instruction : instructions)
        {
            Movement movement = parseMovement(instruction);
            computeMovement(movement, item);
        }
    }

    private void computeMovement(Movement movement, AbstractItem item)
    {
        switch (movement){
            case A:
                calculateNewPosition(item);
                break;
            case D:
                item.rotateRight();
                break;
            case G:
                item.rotateLeft();
                break;
        }
    }

    private void calculateNewPosition(AbstractItem item)
    {
            Position newPosition = item.calculateMovement();
            if(!positions.containsKey(newPosition))
                throw new IllegalArgumentException();
            positions.put(item.getPosition(), null);
            positions.put(newPosition,item);
            item.setPosition(newPosition);
    }

    Map<Position, AbstractItem> getPositionsCopy(){
        return new HashMap<>(positions);
    }

}
