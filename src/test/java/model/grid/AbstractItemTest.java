package model.grid;

import model.enums.Orientation;
import model.grid.item.Vacuum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AbstractItemTest {

    @Test
    void rotateLeftFromNTest(){
        AbstractItem item = new Vacuum(Orientation.N);
        item.rotateLeft();
        Assertions.assertEquals(Orientation.W, item.getOrientation());
    }

    @Test
    void rotateLeftFromWTest(){
        AbstractItem item = new Vacuum(Orientation.W);
        item.rotateLeft();
        Assertions.assertEquals(Orientation.S, item.getOrientation());
    }

    @Test
    void rotateLeftFromSTest(){
        AbstractItem item = new Vacuum(Orientation.S);
        item.rotateLeft();
        Assertions.assertEquals(Orientation.E, item.getOrientation());
    }

    @Test
    void rotateLeftFromETest(){
        AbstractItem item = new Vacuum(Orientation.E);
        item.rotateLeft();
        Assertions.assertEquals(Orientation.N, item.getOrientation());
    }

    @Test
    void rotateRightFromNTest(){
        AbstractItem item = new Vacuum(Orientation.N);
        item.rotateRight();
        Assertions.assertEquals(Orientation.E, item.getOrientation());
    }

    @Test
    void rotateRightFromETest(){
        AbstractItem item = new Vacuum(Orientation.E);
        item.rotateRight();
        Assertions.assertEquals(Orientation.S, item.getOrientation());
    }

    @Test
    void rotateRightFromSTest(){
        AbstractItem item = new Vacuum(Orientation.S);
        item.rotateRight();
        Assertions.assertEquals(Orientation.W, item.getOrientation());
    }

    @Test
    void rotateRightFromWTest(){
        AbstractItem item = new Vacuum(Orientation.W);
        item.rotateRight();
        Assertions.assertEquals(Orientation.N, item.getOrientation());
    }

    @Test
    void calculateMovementNTest(){
        AbstractItem item = new Vacuum(Orientation.N);
        item.setPosition(new Position(5,5));
        Position result = item.calculateMovement();
        Assertions.assertEquals(new Position(5,6), result);
    }

    @Test
    void calculateMovementSTest(){
        AbstractItem item = new Vacuum(Orientation.S);
        item.setPosition(new Position(5,5));
        Position result = item.calculateMovement();
        Assertions.assertEquals(new Position(5,4), result);
    }

    @Test
    void calculateMovementWTest(){
        AbstractItem item = new Vacuum(Orientation.W);
        item.setPosition(new Position(5,5));
        Position result = item.calculateMovement();
        Assertions.assertEquals(new Position(4,5), result);
    }

    @Test
    void calculateMovementETest(){
        AbstractItem item = new Vacuum(Orientation.E);
        item.setPosition(new Position(5,5));
        Position result = item.calculateMovement();
        Assertions.assertEquals(new Position(6,5), result);
    }

}
