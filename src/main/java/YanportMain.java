import model.enums.Orientation;
import model.grid.AbstractItem;
import model.grid.Grid;
import model.grid.item.Vacuum;

import java.util.Scanner;

import static helper.ParseHelper.parseOrientation;


public class YanportMain {

        public static void main(String[] args) {
            try ( Scanner scanner = new Scanner( System.in ) ) {
                System.out.println( "Veuillez saisir la dimension de la grille sur deux chiffres (X Y) : " );
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                Grid grid = new Grid(x, y);
                System.out.println( "La grille est de taille "+x+"x"+y);
                System.out.println( "Veuillez saisir la position initiale de l'aspirateur et son orientation (X Y N) : " );
                int xa = scanner.nextInt();
                int ya = scanner.nextInt();
                Orientation orientation = parseOrientation(scanner.next().charAt(0));
                AbstractItem item = new Vacuum(orientation);
                grid.addItemToGrid(item, xa, ya);
                System.out.println( "La position de l'aspirateur est "+xa+"x"+ya+" et son orientation " + orientation);
                System.out.println( "Veuillez saisir une suite d'instruction (D tourner à droite| G tourner à gauche| A avancer) :");
                String instruction = scanner.next();
                grid.moveItem(item, instruction);
                System.out.println( "l'instruction demandée est "+instruction);
                System.out.println( "La position de l'aspirateur est "+item.getPosition().toString()+" et son orientation "+item.getOrientation());
            }
        }

}
