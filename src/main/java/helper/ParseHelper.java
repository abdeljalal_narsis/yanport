package helper;


import model.enums.Movement;
import model.enums.Orientation;

public class ParseHelper {

    private ParseHelper(){}

    public static Orientation parseOrientation(char orientation) throws IllegalArgumentException {
        Orientation result = Orientation.getOrientationForName(orientation);
        if(result == null)
            throw new IllegalArgumentException("Les orientations possibles sont N S E W");
        return result;
    }

    public static Movement parseMovement(char movement) throws IllegalArgumentException {
        Movement result = Movement.getMovementForName(movement);
        if(result == null)
            throw new IllegalArgumentException("Les mouvements possibles sont  D G A");
        return result;
    }

}
