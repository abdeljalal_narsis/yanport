package helper;

import model.enums.Movement;
import model.enums.Orientation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParseHelperTest {

    @Test
    void parseOrientationNTest(){
        char test ='N';
        Orientation orientation = ParseHelper.parseOrientation(test);
        Assertions.assertEquals(Orientation.N, orientation);
    }

    @Test
    void parseOrientationETest(){
        char test ='E';
        Orientation orientation = ParseHelper.parseOrientation(test);
        Assertions.assertEquals(Orientation.E, orientation);
    }

    @Test
    void parseOrientationSTest(){
        char test ='S';
        Orientation orientation = ParseHelper.parseOrientation(test);
        Assertions.assertEquals(Orientation.S, orientation);
    }

    @Test
    void parseOrientationWTest(){
        char test ='W';
        Orientation orientation = ParseHelper.parseOrientation(test);
        Assertions.assertEquals(Orientation.W, orientation);
    }

    @Test
    void parseOrientationExceptionTest(){
        char test ='Z';
        Assertions.assertThrows(IllegalArgumentException.class, () -> {ParseHelper.parseOrientation(test);});
    }

    @Test
    void parseMovementDTest(){
        char test ='D';
        Movement movement = ParseHelper.parseMovement(test);
        Assertions.assertEquals(Movement.D, movement);
    }

    @Test
    void parseMovementGTest(){
        char test ='G';
        Movement movement = ParseHelper.parseMovement(test);
        Assertions.assertEquals(Movement.G, movement);
    }

    @Test
    void parseMovementATest(){
        char test ='A';
        Movement movement = ParseHelper.parseMovement(test);
        Assertions.assertEquals(Movement.A, movement);
    }

    @Test
    void parseMovementExceptionTest(){
        char test ='Z';
        Assertions.assertThrows(IllegalArgumentException.class, () -> {ParseHelper.parseMovement(test);});
    }

}
