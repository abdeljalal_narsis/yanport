package helper;

import model.grid.Position;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PositionHelperTest {

    @Test
    void calculateNewPositionMoveXTest() {
        Position position = new Position(5,5);
        Position result = PositionHelper.calculateNewPosition(position, 1 ,0);
        Assertions.assertEquals(6, result.getX());
        Assertions.assertEquals(5, result.getY());
    }

    @Test
    void calculateNewPositionMoveYTest() {
        Position position = new Position(5,5);
        Position result = PositionHelper.calculateNewPosition(position, 0 ,1);
        Assertions.assertEquals(5, result.getX());
        Assertions.assertEquals(6, result.getY());
    }

    @Test
    void calculateNewPositionMoveNullPointerTest() {
        Position result = PositionHelper.calculateNewPosition(null, 0 ,1);
        Assertions.assertNull(result);
    }

    @Test
    void toStringTest(){
        Position position = new Position(5,5);
        Assertions.assertEquals("Position{x="+5+", y="+5+"}",position.toString());
    }

}
